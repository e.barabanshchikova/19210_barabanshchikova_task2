package ru.nsu.barabanshchikova.Commands;

import ru.nsu.barabanshchikova.Executor.ExecutionContext;

import java.util.List;

public class Exponentiation extends ExecutableCommand {

    @Override
    public void execute(ExecutionContext context, List<String> inputArgs) {
        double a = context.getStack().pop();
        double b = context.getStack().pop();
        context.getStack().push(Math.pow(a,b));
    }
}
