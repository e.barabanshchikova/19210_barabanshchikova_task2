package ru.nsu.barabanshchikova.Commands;

import ru.nsu.barabanshchikova.Executor.ExecutionContext;

import java.util.List;

public abstract class ExecutableCommand {

    public abstract void execute(ExecutionContext context, List<String> inputArgs);
}
