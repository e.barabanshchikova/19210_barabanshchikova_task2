package ru.nsu.barabanshchikova.Commands;

import ru.nsu.barabanshchikova.Executor.ExecutionContext;
import java.util.List;

public class Pop extends ExecutableCommand {

    @Override
    public void execute(ExecutionContext context, List<String> inputArgs) {
        context.getStack().pop();
    }
}
